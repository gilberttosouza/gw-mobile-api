var express = require('express');

const usersRouter = express.Router(); 

usersRouter.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

usersRouter.route('/:id')
.get(function(req, res){  
  res.json({teste: req.params});
})
.post(function(req, res){
  res.json({teste: req.body});
})
.put(function(req, res){
  res.json({put: 'Put'});
})
.delete(function(req, res){
  db.users.delete({
    where: {
      id: req.params.id
    }
  }).then(function(response){
    res.json(response.data);
  }).catch(function(error){
    res.json(error);
  })
})


module.exports = usersRouter;
